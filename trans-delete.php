<?php
// NOTE: This script should work for both strings and arrays

include 'include/common.php';
$dbh = getDbh();
$editor = getEditor($dbh);
$langId = filter_input(INPUT_GET, 'l', FILTER_VALIDATE_INT);
$keyId = filter_input(INPUT_GET, 'k', FILTER_VALIDATE_INT);
$version = filter_input(INPUT_GET, 'v', FILTER_VALIDATE_INT);
$revision = filter_input(INPUT_GET, 'r', FILTER_VALIDATE_INT);

$translation = getTranslationRevision($dbh, $keyId, $langId, $version, $revision);

if ($langId == DEFAULT_TRANSLATION_ID && $revision == 0 ||
        !$editor->admin && $translation->editor != $editor->id) {
        htmlHead("Delete not allowed", $editor);
        htmlBackLink();
        htmlFoot();
}

deleteTranslationRevision($dbh, $keyId, $langId, $version, $revision);

$key = getStringKey($dbh, $keyId);
if ($key->array) {
    header('Location: trans-array-edit.php?l='.$langId.'&k='.$keyId);
} else {
    header('Location: trans-string-edit.php?l='.$langId.'&k='.$keyId);
}
