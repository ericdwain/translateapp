<?php
include 'include/common.php';
$dbh = getDbh();
// Generates string.xml-files to be included in the app
// Param: l = langId or code=sv/en/whatever
// If code is sent and is empty generate the default file
$langId = filter_input(INPUT_GET, 'l', FILTER_VALIDATE_INT);
if (!$langId) {
    $code = filter_input(INPUT_GET, 'c', FILTER_SANITIZE_STRING);
    $language = getLanguageByCode($dbh, $code);
    if (is_null($language)) {
        error_log("generate: No such language.");
        exit(1);
    }
    $langId = $language->id;
}

$stringKeys = getStringKeys($dbh);
$translations = getTranslationHash($dbh, $langId);
$default = getTranslationHash($dbh, DEFAULT_TRANSLATION_ID);

header("Content-Type: application/xml; charset=utf-8");
header('Content-Disposition: attachment; filename="strings.xml"');

$xmlDoc = new DOMDocument('1.0', 'UTF-8');
$xmlDoc->formatOutput = true;
$xmlRes = $xmlDoc->createElement("resources");
$xmlDoc->appendChild($xmlRes);

// foreach
foreach ($stringKeys as $stringKey) {
    /* @var $stringKey StringKey  */
    if ($langId == 1 || $stringKey->translatable) {
        
        if ($stringKey->array) {

            $xmlArray = $xmlDoc->createElement("string-array");
            if (!$stringKey->translatable) {
                $xmlArray->setAttribute("translatable", "false");
            }
            $xmlArray->setAttribute("name", $stringKey->skey);
            
            $max = getTranslationMaxSorder($dbh, $stringKey->id);
            for ($i = 0; $i <= $max; $i++) {
                $xmlArray->appendChild($xmlDoc->createElement("item", 
                        getRelevantStringEscaped($stringKey->id . '_' . $i, $translations, $default)));
            }
            
            $xmlRes->appendChild($xmlArray);
            
        } else {
            $xmlString = $xmlDoc->createElement("string", 
                    getRelevantStringEscaped($stringKey->id . '_0', $translations, $default));
            if (!$stringKey->translatable) {
                $xmlString->setAttribute("translatable", "false");
            }
            $xmlString->setAttribute("name", $stringKey->skey);
            $xmlRes->appendChild($xmlString);
        }
    }
}

echo $xmlDoc->saveXML();

// &-prefix to pass by reference to avoid data copying
function getRelevantStringEscaped($hashkey, &$wantedHash, &$defaultHash) {
    $string = $defaultHash[$hashkey]->text; // Fallback
    if (isset($wantedHash[$hashkey])) {
        $string = $wantedHash[$hashkey]->text;
    }
    //escape & as stated on http://php.net/manual/en/domdocument.createelement.php
    // escape \ and " and add " first and last. (leave newline and ' as is, when inside ")
    // % and %% is NOT modified here
    return '"'.str_replace('"', '\\"', 
               str_replace('\\', '\\\\', 
               str_replace('&', '&amp;', $string))).'"';
}