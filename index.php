<?php
    include 'include/common.php';
    try {
        $dbh = getDbh();
        $initdb = initDb($dbh);
        $editor = getEditor($dbh);
    } catch (Exception $exc) {
        logError($exc->getTraceAsString());
    }

    htmlHead("Translate ". settingProjectName(), $editor);
    
    if ($initdb) {echo "<p>Database was set up</p>";}

    echo '<ul>';
    foreach (getLanguages($dbh) as $lang) {
        echo '<li><strong>'.$lang->htmlLink().'</strong> '.getTranslationShortStatus($dbh, $lang->id);
        if ($editor && $editor->admin) {                
            echo '<br><a href="export.php?l='.$lang->id.'">Export</a> <a href="import.php?l='.$lang->id.'">Import</a><br>';
        }
        echo '</li>';
    }
    echo '</ul>';
    echo '<p><a href="lang-add.php">Add language</a></p>';

    if ($editor) {                
        echo '<p><a href="check.php">Check placeholders</a></p>';
    }

    
    echo '<p>Contributors</p><ul>';
    foreach (getUsersWithTranslationCount($dbh) as $user) {
        echo '<li>'.$user->username.' - Edits: '.$user->tcount.'</li>';
    }
    echo '</ul>';
    
    echo '<p>Latest edits (Showing last 500 edits)</p><ul>';
    foreach (getUsersWithLastEdits($dbh) as $user) {
        echo '<li>'.$user->tcreated.' '.$user->username.' - Edits: '.$user->tcount.' '.$user->lname.'</li>';
    }
    echo '</ul>';
    
    echo '<footer>This translation tool is using <a href="https://bitbucket.org/erik_melkersson/translateapp">TranslateApp</a></footer>';
    htmlFoot();
 ?>
