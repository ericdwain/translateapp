<?php 
include 'include/common.php';
$dbh = getDbh();
$editor = getEditor($dbh);
htmlHead("Add language", $editor);

$code = filter_input(INPUT_GET, 'code', FILTER_SANITIZE_STRING);

if ($code) {
    $code = preg_replace("/[^_a-zA-Z0-9]+/", "", $code);
    $name = getLangName($code);
    try {
        addLanguage($dbh, $editor->id, $code, $name);
        echo '<p>'.$name.' ('.$code.') added<p>';
    } catch (Exception $e) {
        echo '<p>Cound not add '.$name.' ('.$code.'). Most likely because it is already added.</p>';
    }
} else {
    echo '<form action="lang-add.php" method="GET">';
    echo getLangSelect();
    echo '<br><input type="submit" value="Add"/>';
    echo '</form>';
}
htmlBackLink();
htmlFoot();

// Future DON'T SHOW EXISTING
function getLangSelect() {
    $ret = '<select name="code">';
    $ret .= '<option value="">Select language</option>';
    if (($handle = fopen("lang.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $ret .= '<option value="'.$data[1].'">'.$data[0].' ('.$data[1].')</option>';
        }
        fclose($handle);
    }
    $ret .= '</select>';
    return $ret;
}

function getLangName($code) {
    $name = '';
    if (($handle = fopen("lang.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            if ($data[1] == $code) {
                $name = $data[0];
                break;
            }
        }
        fclose($handle);
    }
    return $name;
}

