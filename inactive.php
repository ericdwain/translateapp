<?php 
include 'include/common.php';
$dbh = getDbh();
$editor = getEditor($dbh);

if (!$editor->admin) {
    htmlHead("You are not allowed to do this. Contact administrator.", $editor);
    htmlBackLink();
    htmlFoot();
    return;
}

$keyId = filter_input(INPUT_GET, 'k', FILTER_VALIDATE_INT);
$confirm = filter_input(INPUT_GET, 'confirm', FILTER_VALIDATE_INT);

if ($confirm === 1) {
    inactivateStringKey($dbh, $keyId);
}
header('Location: lang-edit.php?l=1');