<?php 
include 'include/common.php';
$dbh = getDbh();
$editor = getEditor($dbh);

if (!$editor->admin) {
    htmlHead("You are not allowed to do this. Contact administrator.", $editor);
    htmlBackLink();
    htmlFoot();
    return;
}

$langId = filter_input(INPUT_GET, 'l', FILTER_VALIDATE_INT);

deleteLanguage($dbh, $langId);

header('Location: index.php');