<?php
if (!@include('settings.php')) {
    htmlHead("Setup translate before use");
    echo '<p>Have a look at <a href="https://bitbucket.org/erik_melkersson/translateapp">Setup instructions</a> on what to do first.</p>';
    htmlFoot();
    exit();
}
@include('local.php'); // If any locally defined stuff, like login handling

const DEFAULT_TRANSLATION_ID = 1;
const ADMIN_USER_ID = 1;

function htmlHead($name, $editor = false) {
    if (function_exists('settingTopLogoUrl')) { $topLogoUrl = settingTopLogoUrl(); }
    if (is_null($topLogoUrl)) { $topLogoUrl = 'translate-logo.png'; }
    header('Content-Type: text/html; charset=utf-8');
    echo '<!DOCTYPE html>
<html lang = "en-US">
 <head>
 <meta charset = "UTF-8">
 <meta name="robots" content="noindex, nofollow">
 <title>'.$name.'</title>
 <link rel="stylesheet" type="text/css" href="common.css">
 <link rel="stylesheet" type="text/css" href="style/style.css">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 ';
    if (function_exists('settingGoogleSignInClientId') && settingGoogleSignInClientId()) {
        echo '<meta name="google-signin-client_id" content="'.settingGoogleSignInClientId().'">';
    }
    echo '</head><body>';
    echo '<img src="'.$topLogoUrl.'" class="top-logo">';
    if (function_exists('settingGoogleSignInClientId') && settingGoogleSignInClientId()) {
        if (!$editor) {echo '<div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark"></div>';}
        else {
            echo '<p>Signed in as <b>'.$editor->username.'</b> <a href="#" onclick="signOut();">Sign out</a></p>';
        }
        echo '<script>
function onSignIn(googleUser) {
  window.location.replace("signin.php?idToken=" + googleUser.getAuthResponse().id_token + "&ret=" + encodeURIComponent(location.pathname + location.search));
}
  function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      if (location.pathname.endsWith("signin.php")) window.location.replace("signout.php");
      else window.location.replace("signout.php?&ret=" + encodeURIComponent(location.pathname + location.search));
    });
  }
    function onLoad() {
      gapi.load("auth2", function() {
        gapi.auth2.init();
      });
    }
</script>';
        echo '<script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>'; // Note: Must be last
    }
    
    // Sign out: https://stackoverflow.com/questions/29815870/typeerror-gapi-auth2-undefined
    
    echo '<h1>'.$name.'</h1>';
}

function htmlFoot() {
    echo ' </body>
</html>';
}

function htmlBackLink($langId = 0) {
    echo '<p><a href="';
    if ($langId > 0) {
        echo 'lang-edit.php?l='.$langId;
    } else {
        echo 'index.php';
    }
    echo '">&lt;= Back</a></p>';
}

function logError($msg) {
    ob_start();
    debug_backtrace();
    $err = ob_get_clean();
    ob_end_clean();

    error_log($err);
  
    $date = new DateTime();
    $dateString = $date->format("y-m-d h:i:s");
    error_log($dateString . " SQL ERROR:".$msg."\n".$err."\n", 3, '/tmp/translate-error.log');
}

function getDbh() {
    // Get a database connection
    try {
        $dbArr = settingDbPdoParams();
        $dbh = new PDO($dbArr[0], $dbArr[1], $dbArr[2]); // ($dsn, $dbuser, $dbpassword);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Fail instantly on errors
        return $dbh;
    } catch (PDOException $e) {
        logError("Unable to get database connection:" . $e->getMessage());
        http_response_code(500);
        echo "<html><body>Unable to connect to database</html></body>";
    }
    
}

function initDb($dbh) {
    $wasSetUp = FALSE;
    if (!tableExists($dbh, "editor")) {
        $dbh->exec('CREATE TABLE editor ('.
                'id INT AUTO_INCREMENT PRIMARY KEY,'.
                'account VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,'.
                'created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,'.
                'googleid VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,'.
                'email VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,'.
                'session VARCHAR(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,'.
                'UNIQUE INDEX (account)'.
                ')');
        $stmt = $dbh->prepare("INSERT INTO editor (id, account) VALUES (?,?)");
        $stmt->execute(array(ADMIN_USER_ID, ""));
        $wasSetUp = TRUE;
    }
    if (!tableHasField($dbh, 'editor', 'googleid')) {
        $dbh->exec('ALTER TABLE editor ADD COLUMN googleid VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL');
        $dbh->exec('ALTER TABLE editor ADD COLUMN email VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL');
        $dbh->exec('ALTER TABLE editor ADD COLUMN session VARCHAR(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL');
        $wasSetUp = TRUE;
    }
    if (!tableExists($dbh, "language")) {
        $dbh->exec("CREATE TABLE language ("
                . "id INT AUTO_INCREMENT PRIMARY KEY,"
                . "code VARCHAR(7) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,"
                . 'name VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,'
                . 'created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,'
                . 'editor INT NOT NULL,'
                . 'UNIQUE INDEX (code),'
                . "INDEX (editor),"
                . "FOREIGN KEY (editor) REFERENCES editor(id)"
                . ")");
        $stmt = $dbh->prepare("INSERT INTO language (editor, id, code, name) VALUES (?,?,?,?)");
        $stmt->execute(array(ADMIN_USER_ID, DEFAULT_TRANSLATION_ID, "DEFAULT", "DEFAULT")); // default language
        $wasSetUp = TRUE;
    }
    if (!tableExists($dbh, "stringkey")) {
        $dbh->exec('CREATE TABLE stringkey ('.
                'id INT AUTO_INCREMENT PRIMARY KEY,'.
                'translatable BOOLEAN NOT NULL,'.
                'array BOOLEAN NOT NULL,'.
                'skey VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,'.
                'description VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,'.
                'active BOOLEAN DEFAULT TRUE,'.
                'lastversion INT NOT NULL,'.
                'UNIQUE INDEX (skey)'.
                ')');
        $wasSetUp = TRUE;
    }
    if (!tableHasField($dbh, 'stringkey', 'description')) {
        $dbh->exec('ALTER TABLE stringkey ADD COLUMN description VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci');
        $wasSetUp = TRUE;
    }
    if (!tableHasField($dbh, 'stringkey', 'active')) {
        $dbh->exec('ALTER TABLE stringkey ADD COLUMN active BOOLEAN DEFAULT TRUE');
        $wasSetUp = TRUE;
    }
    if (!tableExists($dbh, "translation")) {
        $dbh->exec("CREATE TABLE translation ("
                . "id INT NOT NULL,"
                . "sorder INT NOT NULL,"
                . "lang INT NOT NULL,"
                . "version INT NOT NULL," // requires new translation
                . "revision INT NOT NULL," // does NOT require new translation, also used for updates on translated strings on same version
                . 'created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,'
                . 'editor INT NOT NULL,'
                . "text VARCHAR(16000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,"
                . "UNIQUE INDEX (id,sorder,lang,version,revision),"
                . "INDEX (editor),"
                . "FOREIGN KEY (id) REFERENCES stringkey(id) ON DELETE CASCADE,"
                . "FOREIGN KEY (lang) REFERENCES language(id),"
                . "FOREIGN KEY (editor) REFERENCES editor(id)"
                . ");");     
        $wasSetUp = TRUE;
    }
    return $wasSetUp;
}

// Note not checking that $table is safe
function tableExists($dbh, $table) {
    $result = $dbh->query("SHOW TABLES LIKE '$table'");
    return $result !== false && $result->rowCount() > 0;
}
// Note not checking that $table/$field is safe
function tableHasField($dbh, $table, $field) {
    $result = $dbh->query("SHOW COLUMNS FROM $table LIKE '$field'");
    return $result !== false && $result->rowCount() > 0;    
}

function getSessionCookieName() {
    if (function_exists("settingCookieName")) {
        return settingCookieName();
    }
    return "TRANSLATEAPP";
}

/////////////////
// Editor

class User {
    public $username;
    public $id;
    public $googleid;
    public $admin;
    public $tcount; // used for translations count and last translations
    public $tcreated; // used for last translation made
    public $lname; // language name, used for last translation made
}

function getEditor($dbh) {
    if (function_exists('local_getEditor')) {
        return local_getEditor($dbh);
    }
    if (isset($_COOKIE[getSessionCookieName()])) {
        $user = getEditorBySession($dbh, $_COOKIE[getSessionCookieName()]);
        if (function_exists("settingIsAdminGoogleId")) {
            $user->admin = settingIsAdminGoogleId($user->googleid);
        }
        return $user;
    } else {
        $username = $_SERVER['PHP_AUTH_USER'];
        if (!$username) { $username = $_SERVER['REMOTE_USER'];}
        if (!$username) { return null; }
        $user = new User();
        $user->id = getEditorId($dbh, $username);
        $user->username = $username;
        $user->admin = settingIsAdmin($username);
        return $user;
    }
}

function deleteSession($dbh, $session) {
    $stmt = $dbh->prepare('UPDATE editor SET session=NULL WHERE session=?');
    $stmt->execute(array($session));
}

function getEditorBySession($dbh, $session) {
    $stmt = $dbh->prepare('SELECT id,googleid,account AS username FROM editor WHERE session=?');
    $stmt->execute(array($session));
    $stmt->setFetchMode(PDO::FETCH_CLASS, 'User');
    return $stmt->fetch();
}

// Note: Assumes the use is logged in as $account
// Return id(>0) or 0 if the user is blocked
function getEditorId($dbh, $account) {
    // Find existing one
    $stmt = $dbh->prepare("SELECT id FROM editor WHERE account = ?"); //Future: check if blocked
    $stmt->execute(array($account));
    $id = $stmt->fetchColumn();
    if ($id) { return $id; }
    // Create it
    $stmtI = $dbh->prepare("INSERT INTO editor (account) VALUES (?)");
    $stmtI->execute(array($account));
    return $dbh->lastInsertId();
}

// Note: Assumes the use is logged in as $account
// Return id(>0) or 0 if the user is blocked
function getEditorSessionByGoogleId($dbh, $googleId, $name, $email, $autoCreate) {
    // Generate a random string
    $session = sprintf('%04x%04x%04x%04x%04x%04x%04x%04x%04x', 
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), 
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), 
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
    // Find existing one
    $stmt = $dbh->prepare("UPDATE editor SET session = ? WHERE googleId = ?");
    $stmt->execute(array($session, $googleId));
    if ($stmt->rowCount() > 0) { return $session; }
    if (function_exists('settingGetAccountNameFromGoogleId')) {
        $name = settingGetAccountNameFromGoogleId($googleId);
        if (!$name) { return null; } // User was not ok
        try {
            createAccountAndSetSession($dbh, $googleId, $name, $email, $session);
        } catch (Exception $e) {
            echo $name . ' is already occupied. Please contact administrator.';
            exit();
        }
        return $session;
    } else if ($autoCreate) { 
        createAccountAndSetSession($dbh, $googleId, $name, $email, $session);
        return $session;
    }
    return null; 
}

function createAccountAndSetSession($dbh, $googleId, $name, $email, $session) {
    // Create it
    $stmtI = $dbh->prepare("INSERT INTO editor (account,googleid,email,session) VALUES (?,?,?,?)");
    $stmtI->execute(array($name,$googleId,$email,$session));
}


function getUsersWithTranslationCount($dbh) {
    $stmt = $dbh->prepare('SELECT e.id,e.username,GROUP_CONCAT(CONCAT(tcount, " ", l.name) SEPARATOR ", ") AS tcount FROM (SELECT e.id,e.account AS username,count(t.id) as tcount,t.lang FROM editor AS e LEFT JOIN translation AS t ON e.id=t.editor GROUP BY e.id,t.lang HAVING tcount > 0) AS e LEFT JOIN language AS l ON e.lang=l.id GROUP BY e.id');
    //$stmt = $dbh->prepare('SELECT e.id,e.account AS username,count(t.id) as tcount FROM editor AS e LEFT JOIN translation AS t ON e.id=t.editor GROUP BY e.id HAVING tcount > 0');
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS, "User");
}

// Return a list of last edits by a user
// editor, datetime
function getUsersWithLastEdits($dbh) {
    $stmt = $dbh->prepare('SELECT e.id,e.account AS username,l.name AS lname,MAX(t.edited) as tcreated,count(t.id) AS tcount FROM (SELECT DISTINCT id,lang,editor,DATE(created) AS edited FROM `translation` ORDER BY created DESC LIMIT 500) AS t LEFT JOIN editor AS e ON t.editor=e.id LEFT JOIN language AS l ON t.lang=l.id GROUP BY t.editor,t.lang ORDER BY tcreated DESC');  
//    $stmt = $dbh->prepare('SELECT e.id,e.account AS username,max(t.created) as tcreated FROM editor AS e JOIN translation AS t ON e.id=t.editor GROUP BY e.id ORDER BY tcreated DESC LIMIT 5');
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS, "User");
}

/////////////////
// Language 

class Language {
    public $id;
    public $code;
    public $name;
    public function htmlLink() {
        return sprintf('<a href="lang-edit.php?l=%s">%s (%s)</a>',$this->id,$this->name,$this->code);
    }
    public function nameAndCode() {
        return sprintf('%s (%s)',$this->name,$this->code);
    }
}

function getLanguages($dbh) {
    $stmt = $dbh->prepare("SELECT * FROM language ORDER BY id");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS, "Language");
}

function getLanguage($dbh, $id) {
    $stmt = $dbh->prepare("SELECT * FROM language WHERE id = ?");
    $stmt->execute(array($id));
    $stmt->setFetchMode(PDO::FETCH_CLASS, 'Language');
    return $stmt->fetch();
}

function getLanguageByCode($dbh, $code) {
    $stmt = $dbh->prepare("SELECT * FROM language WHERE code = ?");
    $stmt->execute(array($code));
    $stmt->setFetchMode(PDO::FETCH_CLASS, 'Language');
    return $stmt->fetch();
}

function addLanguage($dbh, $editorId, $code, $name) {
    $stmt = $dbh->prepare("INSERT INTO language (editor, code, name) VALUES (?,?,?)");
    $stmt->execute(array($editorId, $code, $name));
    return $dbh->lastInsertId();
}

// Note: This SHALL fail if there are translations left
function deleteLanguage($dbh, $id) {
    $stmt = $dbh->prepare("DELETE FROM language WHERE id = ?");
    $stmt->execute(array($id));
    return $dbh->lastInsertId();
}

///////////////
// key

class StringKey {
    public $id;
    public $skey;
    public $translatable;
    public $array;
    public $lastversion;
    public $description;
}

function addStringKey($dbh, $stringkey, $translatable, $array, $description) {
    $translatable = $translatable ? 1 : 0; // Set useful type for db
    $array = $array ? 1 : 0; // Set useful type for db
    $stmt = $dbh->prepare("INSERT INTO stringkey (skey, translatable, array, lastversion, description) VALUES (?,?,?,0,?)");
    $stmt->execute(array($stringkey, $translatable, $array, $description));
    return $dbh->lastInsertId();
}

function getStringKeys($dbh) {
    $stmt = $dbh->prepare("SELECT * FROM stringkey WHERE active=TRUE ORDER BY skey");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS, "StringKey");
}

function getStringKey($dbh, $keyId) {
    $stmt = $dbh->prepare("SELECT * FROM stringkey WHERE id=?");
    $stmt->execute(array($keyId));
    $stmt->setFetchMode(PDO::FETCH_CLASS, 'StringKey');
    return $stmt->fetch();
}

function updateLastVersion($dbh, $keyId) {
    $stringKey = getStringKey($dbh, $keyId);
    /* @var $stringKey StringKey */
    $newVersion = $stringKey->lastversion + 1;
    $stmt = $dbh->prepare("UPDATE stringkey SET lastversion = ? WHERE id=?");
    $stmt->execute(array($newVersion, $keyId));
    return $newVersion;
}

// Gives a hash based on key
function getStringKeyHash($dbh) {
    $stmt = $dbh->prepare("SELECT * FROM stringkey");
    $stmt->execute();
    $stmt->setFetchMode(PDO::FETCH_CLASS, 'StringKey');
    $hash = array();
    while ($stringKey = $stmt->fetch()) {
        $hash[$stringKey->skey] = $stringKey;
    }
    return $hash;
}

function setStringKeyTranslatable($dbh, $keyId, $translatable) {
    $translatable = $translatable ? 1 : 0; // Set useful type for db
    $stmt = $dbh->prepare("UPDATE stringkey SET translatable = ? WHERE id=?");
    $stmt->execute(array($translatable, $keyId));
}

function setStringKeyArray($dbh, $keyId, $translatable) {
    $stmt = $dbh->prepare("UPDATE stringkey SET array = ? WHERE id=?");
    $stmt->execute(array($translatable, $keyId));
}

function setStringKeyDescription($dbh, $keyId, $description) {
    $stmt = $dbh->prepare("UPDATE stringkey SET description = ? WHERE id=?");
    $stmt->execute(array($description, $keyId));
}

function setStringKeyName($dbh, $keyId, $stringkey) {
    $stmt = $dbh->prepare("UPDATE stringkey SET skey = ? WHERE id=?");
    $stmt->execute(array($stringkey, $keyId));
}

function inactivateStringKey($dbh, $keyId) {
    $stmt = $dbh->prepare("UPDATE stringkey SET active = FALSE WHERE id=?");
    $stmt->execute(array($keyId));
}

////////////////
// Translations

class Translation {
    public $id;
    public $sorder;
    public $lang;
    public $langname;
    public $text;
    public $version;
    public $revision;
    public $created;
    public $editor;
    public $editorname;
}

// Note: Return a hash based on id_sorder and sorder is 0 on strings and can have a value in arrays
// Only return active versions
function getTranslationHash($dbh, $langId) {
    $hash = array();
    $stmt = $dbh->prepare("SELECT t.* FROM stringkey AS k LEFT JOIN translation AS t ON k.id=t.id AND k.lastversion=t.version WHERE lang = ? AND k.active=TRUE");
    //$stmt = $dbh->prepare("SELECT * FROM translation WHERE lang = ?");
    $stmt->execute(array($langId));
    $stmt->setFetchMode(PDO::FETCH_CLASS, 'Translation');
    while ($translation = $stmt->fetch()) {
        /* @var $translation Translation  */
        $hashkey = $translation->id .'_'. $translation->sorder;
        if (isset($hash[$hashkey])) {
            // Only get the last version/revision - TODO Do this in sql instead
            if (/*$translation->version > $hash[$hashkey]->version || 
                    $translation->version == $hash[$hashkey]->version && */
                    $translation->revision > $hash[$hashkey]->revision) {
                $hash[$hashkey] = $translation;
            }
        } else {
            $hash[$hashkey] = $translation;
        }
    }
    return $hash;
}

/* Get the last revision of that version. Note may be null. */
function getTranslation($dbh, $keyId, $langId, $version) {
    $stmt = $dbh->prepare("SELECT * FROM translation WHERE id = ? AND lang = ? AND version = ? ORDER BY revision DESC LIMIT 1");
    $stmt->execute(array($keyId, $langId, $version));
    $stmt->setFetchMode(PDO::FETCH_CLASS, 'Translation');
    return $stmt->fetch();
}
function getTranslationRevision($dbh, $keyId, $langId, $version, $revision) {
    $stmt = $dbh->prepare("SELECT * FROM translation WHERE id = ? AND lang = ? AND version = ? AND revision = ?");
    $stmt->execute(array($keyId, $langId, $version,$revision));
    $stmt->setFetchMode(PDO::FETCH_CLASS, 'Translation');
    return $stmt->fetch();
}

// Note: for both strings and arrays
function deleteTranslationRevision($dbh, $keyId, $langId, $version, $revision) {
    $stmt = $dbh->prepare("DELETE FROM translation WHERE id = ? AND lang = ? AND version = ? AND revision = ?");
    return $stmt->execute(array($keyId, $langId, $version,$revision));
}

function addTranslation($dbh, $editorId, $keyId, $langId, $version, $revision, $text) {
    addArrayTranslation($dbh, $editorId, $keyId, 0, $langId, $version, $revision, $text);
}

function addArrayTranslation($dbh, $editorId, $keyId, $sorder, $langId, $version, $revision, $text) {
    $stmt = $dbh->prepare("INSERT INTO translation (editor, id, sorder, lang, version, revision, text) VALUES (?,?,?,?,?,?,?)");
    $stmt->execute(array($editorId, $keyId, $sorder, $langId, $version, $revision, $text));
}

function getTranslationHistory($dbh, $keyId, $langId) {
//    SELECT t.*,e.account AS editorname FROM translation AS t LEFT JOIN editor AS e ON t.editor=e.id 
//    WHERE t.id=917 AND (lang = 7 OR lang = 1) ORDER BY lang DESC, version DESC, revision DESC;

    $stmt = $dbh->prepare("SELECT t.*,e.account AS editorname,l.name AS langname FROM translation AS t"
            . " LEFT JOIN editor AS e ON t.editor=e.id"
            . " LEFT JOIN language AS l ON t.lang=l.id"
            . " WHERE t.id=? AND (t.lang = ? OR t.lang = ".DEFAULT_TRANSLATION_ID.") ORDER BY lang DESC, version DESC, revision DESC");
//    $stmt = $dbh->prepare("SELECT * FROM translation WHERE id=? AND (lang = ? OR lang = ".DEFAULT_TRANSLATION_ID.") ORDER BY lang DESC, version DESC, revision DESC");
    $stmt->execute(array($keyId, $langId));
    return $stmt->fetchAll(PDO::FETCH_CLASS, "Translation");
}

function getTranslationShortStatus($dbh, $langId) {
    return round(100 * getTranslationCompleteness($dbh, $langId), 1).'% complete, ' . 
           round(100 * getTranslationUpdated($dbh, $langId), 1).'% updated' ;
}

// Return a percent number of how complete translation is to a language
function getTranslationCompleteness($dbh, $langId) {
    $stmt = $dbh->prepare("SELECT count(DISTINCT t.id) / count(DISTINCT k.id) FROM stringkey AS k LEFT JOIN translation AS t ON k.id=t.id AND t.lang=? WHERE k.translatable=TRUE AND k.active=TRUE");
    $stmt->execute(array($langId));
    return $stmt->fetchColumn();
}

// Return a percent number of how updated a translation is to a language
function getTranslationUpdated($dbh, $langId) {
    $stmt = $dbh->prepare("SELECT count(DISTINCT t.id) / count(DISTINCT k.id) FROM stringkey AS k LEFT JOIN translation AS t ON k.id=t.id AND t.lang=? AND k.lastversion=t.version WHERE k.translatable=TRUE AND k.active=TRUE");
    $stmt->execute(array($langId));
    return $stmt->fetchColumn();
}

// Note : Always checks latest version on default language
function getTranslationMaxSorder($dbh, $keyId) {
    $stmt = $dbh->prepare("SELECT max(t.sorder) FROM stringkey AS k LEFT JOIN translation AS t ON k.id=t.id AND k.lastversion=t.version WHERE lang = 1 AND k.id=?");
    $stmt->execute(array($keyId));
    return $stmt->fetchColumn();    
}
